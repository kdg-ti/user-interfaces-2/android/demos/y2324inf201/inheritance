package inkotlin

import injava.Weekend
import java.time.DayOfWeek
import java.time.LocalDate

private fun isWeekend(): String {
    val today = LocalDate.now().dayOfWeek
    return if (today == DayOfWeek.SATURDAY || today == DayOfWeek.SUNDAY) "weekend" else "work"
}

fun main() {
    println(isWeekend())
}