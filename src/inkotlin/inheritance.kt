package inkotlin

import kotlin.math.PI

interface Shape{
    fun getArea(): Double
}

open class Circle(open var radius: Double ) : Shape{
//    var radius:Double
//
//constructor( radius:Double){
//    this.radius = radius
//}
    var diameter
        get() = radius + radius
        set(value:Double) {radius=value/2}


    override fun getArea(): Double {
        return radius * radius * PI
    }
}

class ColouredCircle(override var radius: Double, var colour :String): Circle(radius) {
}



fun main(){
    println (Circle(3.0).getArea())
}