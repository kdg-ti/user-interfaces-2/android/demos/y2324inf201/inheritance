package injava;

import java.time.DayOfWeek;
import java.time.LocalDate;

public class Weekend {
	public static void main(String[] args) {
		System.out.println(isWeekend());
	}

	private static String isWeekend() {
		DayOfWeek today = LocalDate.now().getDayOfWeek();
	return (today == DayOfWeek.SATURDAY ||today == DayOfWeek.SUNDAY)? "weekend" : "work";
	}
}
